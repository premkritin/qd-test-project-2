
# # ###### eks cluster ################################

 instance_types  = ["t3.small"]
 node_group_name = "eks-worker-node"
 cluster_name    = "eks-dev-test-cluster"
 eks_version     = "1.19"

